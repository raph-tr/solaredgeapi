<?php

use App\Http\Controllers\SolarSiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SolarSiteController::class, 'index'])->name('home');

Route::get('/solarSiteDetails/{id:uuid}', [SolarSiteController::class, 'getSiteDetails'])->name('solarSiteDetails');

Route::get('/updateSolarSites', function(){
    return view('updateSolarSites');
})->name('updateSolarSites');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
