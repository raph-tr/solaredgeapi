<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SolarSite;

class SolarSiteController extends Controller
{
    public function index()
    {
        //
        $solarSites = SolarSite::orderBy('started', 'ASC')
                          ->get();
        return view('home', compact('solarSites') );
    }

    public function getSiteDetails(int $id){
        $solarSite = SolarSite::findOrFail($id);
        $solarSiteDetails = file_get_contents("https://monitoringapi.solaredge.com/site/".$solarSite->solarEdgeId."/overview.json?api_key=".$solarSite->solarEdgeApiKey );
        $solarSiteEnv = file_get_contents("https://monitoringapi.solaredge.com/site/".$solarSite->solarEdgeId."/envBenefits.json?api_key=".$solarSite->solarEdgeApiKey );
        $solarSiteDetails = substr( $solarSiteDetails, 0, -1 ) ;
        $solarSiteEnv = substr( $solarSiteEnv, 1 );
        $solarSiteDetails =  $solarSiteDetails . ', ' . $solarSiteEnv ;
        echo $solarSiteDetails;
//        return view('solarSiteDetails', [ 'details'=>$solarSiteDetails ]);
    }
}
