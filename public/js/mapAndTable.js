var map = L.map('map').setView([45.25, 5.45], 7);
var OpenStreetMap_France = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    maxZoom: 20,
    attribution: '&copy; OpenStreetMap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
OpenStreetMap_France.addTo(map);

var pannelIcon = L.icon({
    iconUrl: '{{ URL::to('/') }}/img/icon.png',
    iconSize: [50, 36],
    iconAnchor: [25, 18],
    popupAnchor: [-3, -14],
});

var bounds = L.latLngBounds();
var totalEnergy = 0;
var totalPower = 0;
var totalCo2 = 0;
var totalTrees = 0;

@foreach($solarSites as $solarSite)
popup = '<strong>{{$solarSite->name}}</strong><br /> Pc = {{$solarSite->peakPower}} kWc'
    + '<br />installé : {{$solarSite->started}}<br />'
    + '<a href="{{$solarSite->publicDashboardUrl}}" target="_blank">tableau de bord</a>';

siteMarker{{ $solarSite->id }} = L.marker([ {{ $solarSite->lat }}, {{ $solarSite->lng }} ],  { icon: pannelIcon }).bindPopup(popup).addTo(map);

bounds.extend([ {{ $solarSite->lat }}, {{ $solarSite->lng }} ]);

tr = '<tr siteId="{{$solarSite->id}}" lat="{{ $solarSite->lat }}" lng="{{ $solarSite->lng }}">' +
    '   <td id="name-{{$solarSite->id}}"><strong>{{$solarSite->name}}</strong><br /><small>{{$solarSite->peakPower}} kWc - <a href="{{$solarSite->publicDashboardUrl}}" target="_blank">tableau de bord</a></small> </td>' +
    '   <td id="energy-{{$solarSite->id}}">...</td>' +
    '   <td id="power-{{$solarSite->id}}">...</td>' +
    '   <td id="co2-{{$solarSite->id}}">...</td>' +
    '   <td id="trees-{{$solarSite->id}}">...</td>' +
    '</tr>';
$("#results-tbody").append(tr);

@endforeach

map.fitBounds(bounds,300);

@foreach($solarSites as $site)
$.getJSON("{{ URL::to('/') }}/solarSiteDetails/{{$site->id}}",  function ( data ) {
    $("#energy-"+{{$site->id}}).html(Math.round(data.overview.lastYearData.energy/100)/10+" kWh");
    $("#power-"+{{$site->id}}).html(Math.round(data.overview.currentPower.power/100)/10+" kW");
    $("#co2-"+{{$site->id}}).html(Math.round(data.envBenefits.gasEmissionSaved.co2*10)/10+" Kg équ CO<sub>2</sub>");
    $("#trees-"+{{$site->id}}).html(Math.round(data.envBenefits.treesPlanted*10)/10);
    totalEnergy += data.overview.lastYearData.energy;
    totalPower += data.overview.currentPower.power;
    totalCo2 += data.envBenefits.gasEmissionSaved.co2;
    totalTrees += data.envBenefits.treesPlanted;
    $("#energy-total").html(Math.round(totalEnergy/100)/10+" kWh");
    $("#power-total").html(Math.round(totalPower/100)/10+" kW");
    $("#co2-total").html(Math.round(totalCo2/100)/10+" Tons");
    $("#trees-total").html(Math.round(totalTrees*10)/10+" trees");
});
@endforeach


$("tr").click( function(){
    map.flyTo( [$(this).attr('lat'), $(this).attr('lng')], 13);

} );
