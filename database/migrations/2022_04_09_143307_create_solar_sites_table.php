<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solar_sites', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->float('lat');
            $table->float('lng');
            $table->string('name');
            $table->integer('solarEdgeId');
            $table->string('solarEdgeApiKey');
            $table->float('peakPower');
            $table->timestamp('started');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solar_sites');
    }
};
