<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Suivi des installations</title>

        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
              integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
              crossorigin="" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet" />

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.dark\:text-gray-500{--tw-text-opacity:1;color:#6b7280;color:rgba(107,114,128,var(--tw-text-opacity))}}
        </style>

        <link rel="stylesheet" href="{{asset('css/css.css')}}">

    </head>
    <body class="antialiased">

        <div id="map"></div>

        <table id="results-table">
            <thead>
            <tr>
                <th></th>
                <th>Production<br />année en cours</th>
                <th>Puissance<br />instantanée</th>
                <th>Réduction<br />émissions CO<sub>2</sub></th>
                <th>Equivalent<br />arbres plantés</th>
            </tr>
            </thead>
            <tbody id="results-tbody">
            </tbody>
            <tfoot>
                <tr id="result_total">
                    <td id="name-total"><strong>Total</strong></td>
                    <td id="energy-total"></td>
                    <td id="power-total"></td>
                    <td id="co2-total"></td>
                    <td id="trees-total"></td>
                </tr>
            </tfoot>
        </table>


        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
                integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
                crossorigin="">
        </script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous">
        </script>

        <script>

            function highlightRow(id) {
                $("tbody>tr").css({'background-color':'white'});
                $("tr[siteId="+id+"]").css({'background-color':'#dbffdd'});
            };

            var map = L.map('map').setView([45.25, 5.45], 7);
            var OpenStreetMap_France = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
                maxZoom: 20,
                attribution: '&copy; OpenStreetMap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            });
            OpenStreetMap_France.addTo(map);

            var pannelIcon = L.icon({
                iconUrl: '{{ URL::to('/') }}/img/icon.png',
                iconSize: [50, 36],
                iconAnchor: [25, 18],
                popupAnchor: [-3, -14],
            });

            // create a markers object
            var markersObject = {};

            var bounds = L.latLngBounds();
            var totalEnergy = 0;
            var totalPower = 0;
            var totalCo2 = 0;
            var totalTrees = 0;

            @foreach($solarSites as $solarSite)
                popup = '<strong>{{$solarSite->name}}</strong><br /> Pc = {{$solarSite->peakPower}} kWc'
                + '<br />installé : {{$solarSite->started}}<br />'
                + '<a href="{{$solarSite->publicDashboardUrl}}" target="_blank">tableau de bord</a>';

            siteMarker = L.marker([ {{ $solarSite->lat }}, {{ $solarSite->lng }} ],  { icon: pannelIcon })
                .bindPopup(popup)
                .addTo(map)
                .on('click', function(){
                    highlightRow({{ $solarSite->id }})
                    map.flyTo( [{{ $solarSite->lat }}, {{ $solarSite->lng }} ], 14);
                });

            markersObject["{{$solarSite->id}}"] = siteMarker;

            bounds.extend([ {{ $solarSite->lat }}, {{ $solarSite->lng }} ]);

            tr = '<tr siteId="{{$solarSite->id}}" lat="{{ $solarSite->lat }}" lng="{{ $solarSite->lng }}">' +
                '   <td id="name-{{$solarSite->id}}"><strong>{{$solarSite->name}}</strong><br /><small>{{$solarSite->peakPower}} kWc - <a href="{{$solarSite->publicDashboardUrl}}" target="_blank">tableau de bord</a></small> </td>' +
                '   <td id="energy-{{$solarSite->id}}"> <div class="waviy"><span style="--i:1">.</span><span style="--i:2">.</span><span style="--i:3">.</span></div></td>' +
                '   <td id="power-{{$solarSite->id}}"><div class="waviy"><span style="--i:1">.</span><span style="--i:2">.</span><span style="--i:3">.</span></div></td>' +
                '   <td id="co2-{{$solarSite->id}}"><div class="waviy"><span style="--i:1">.</span><span style="--i:2">.</span><span style="--i:3">.</span></div></td>' +
                '   <td id="trees-{{$solarSite->id}}"><div class="waviy"><span style="--i:1">.</span><span style="--i:2">.</span><span style="--i:3">.</span></div></td>' +
                '</tr>';
            $("#results-tbody").append(tr);

            @endforeach

            map.fitBounds(bounds,300);

            @foreach($solarSites as $site)
            $.getJSON("{{ URL::to('/') }}/solarSiteDetails/{{$site->id}}",  function ( data ) {
                $("#energy-"+{{$site->id}}).html(Math.round(data.overview.lastYearData.energy/1000)+" kWh");
                $("#power-"+{{$site->id}}).html(Math.round(data.overview.currentPower.power/1000)+" kW");
                $("#co2-"+{{$site->id}}).html(Math.round(data.envBenefits.gasEmissionSaved.co2)+" Kg équ CO<sub>2</sub>");
                $("#trees-"+{{$site->id}}).html(Math.round(data.envBenefits.treesPlanted*10)/10);
                totalEnergy += data.overview.lastYearData.energy;
                totalPower += data.overview.currentPower.power;
                totalCo2 += data.envBenefits.gasEmissionSaved.co2;
                totalTrees += data.envBenefits.treesPlanted;
                $("#energy-total").html(Math.round(totalEnergy/100000)/10+" MWh");
                $("#power-total").html(Math.round(totalPower/100)/10+" kW");
                $("#co2-total").html(Math.round(totalCo2/100)/10+" Tones equ CO<sub>2</sub>");
                $("#trees-total").html(Math.round(totalTrees*10)/10+" arbres");
            });
            @endforeach


            $("tr").click( function(){
                map.flyTo( [$(this).attr('lat'), $(this).attr('lng')], 14);
                markersObject[$(this).attr('siteId')].openPopup();
                highlightRow($(this).attr('siteId'));
            } );

        </script>

        </script>
    </body>
</html>
